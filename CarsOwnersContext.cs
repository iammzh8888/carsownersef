﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsOwnersEF
{
    class CarsOwnersContext : DbContext
    {
        private const string databaseName = "CarsOwnersDatabase.mdf";
        private static string DbPath = Path.Combine(Environment.CurrentDirectory, databaseName);

        public CarsOwnersContext() : base(
            $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30")
        {

        }

        public DbSet<CarsOwner> CarsOwnerEntity { get; set; }
        public DbSet<Car> CarEntity { get; set; }
    }
}
