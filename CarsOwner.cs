﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CarsOwnersEF
{
    public class CarsOwner
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public Car[] Cars { get; set; }
        public byte[] Photo { get; set; }

        public CarsOwner()
        {
        }
        public CarsOwner(string name)
        {
            this.Name = name;
        }

        public CarsOwner(string name, byte[] img)
        {
            this.Name = name;
            this.Photo = img;
        }

        public CarsOwner(string name, Car[] cars, byte[] img)
        {
            this.Name = name;
            this.Cars = cars;
            this.Photo = img;
        }
    }

    public class Car
    {
        [Key]
        public int CarId { get; set; }
        [Required]
        [StringLength(50)]
        public string MakeModel { get; set; }
        public Car(){}

        public Car(string makeModel)
        {
            this.MakeModel = makeModel;
        }
    }
}
