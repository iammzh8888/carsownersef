﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Path = System.IO.Path;

namespace CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static readonly CarsOwnersContext Coc = new CarsOwnersContext();
        public MainWindow()
        {
            InitializeComponent();

            LoadFile();
        }
        private void LoadFile()
        {
            LvCarsOwners.ItemsSource = Coc.CarsOwnerEntity.ToList<CarsOwner>();
            string imgName = @"sky.JPG";
            string imgPath = Path.Combine(Environment.CurrentDirectory, imgName);
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(imgPath);
            bitmap.EndInit();
            ImgCar.Source = bitmap;
            ResetValue();
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = "";
            if (TextName.Text != "")
            {
                name = TextName.Text;
            }
            else
            {
                SendMessage("Name must be filled");
                return;
            }

            if (ImgCar.Source != null)
            {
                string fileArray = ImgCar.Source.ToString().TrimStart('f', 'i', 'l', 'e', ':','/', '/', '/');
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(fileArray);
                bitmap.EndInit();
                byte[] imgBytes = BitmapImageToByteArray(bitmap);
                CarsOwner c = new CarsOwner(name, imgBytes);
                //Car car1 = new Car();
                //car1.MakeModel = "Honda";
                //car1.Count = 1;
                //Car car2 = new Car();
                //car2.MakeModel = "Toyota";
                //car2.Count = 1;
                //Car car3 = new Car();
                //car3.MakeModel = "Ford";
                //car3.Count = 1;
                //c.Cars = new Car[] { car1, car2, car3 };
                Coc.CarsOwnerEntity.Add(c);
            }
            else
            {
                CarsOwner c = new CarsOwner(name);
                Coc.CarsOwnerEntity.Add(c);
            }



            Coc.SaveChanges();
            ResetValue();
        }

        private void ResetValue()
        {
            LvCarsOwners.ItemsSource = Coc.CarsOwnerEntity.ToList<CarsOwner>();
            LvCarsOwners.Items.Refresh();
            TextName.Clear();
            LblId.Content = "";
            LvCarsOwners.SelectedIndex = -1;
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (LvCarsOwners.SelectedIndex == -1)
            {
                SendMessage("you need to choose one CarsOwner");
                return;
            }

            string name = "";
            if (TextName.Text != "")
            {
                name = TextName.Text;
            }
            else
            {
                SendMessage("Name must be filled");
                return;
            }

            CarsOwner carsOwnerTobeUpdated = (CarsOwner)LvCarsOwners.SelectedItem;
            carsOwnerTobeUpdated.Name = name;

            if (ImgCar.Source != null)
            {
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(ImgCar.Source.ToString());
                bitmap.EndInit();
                byte[] imgBytes = BitmapImageToByteArray(bitmap);
                carsOwnerTobeUpdated.Photo = imgBytes;
            }

            Coc.SaveChanges();
            //ResetValue();
            LvCarsOwners.ItemsSource = Coc.CarsOwnerEntity.ToList<CarsOwner>();
            LvCarsOwners.Items.Refresh();
        }

        private void LvCarsOwners_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BtnAdd.IsEnabled = true;
            BtnUpdate.IsEnabled = true;

            var selectedCarsOwner = LvCarsOwners.SelectedItem;

            if (selectedCarsOwner is CarsOwner)
            {
                CarsOwner co = (CarsOwner)LvCarsOwners.SelectedItem;
                LblId.Content = co.Id.ToString();
                TextName.Text = co.Name;
                if (co.Photo != null)
                {
                    ImgCar.Source = ByteArrayToBitmapImage(co.Photo);
                }
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (LvCarsOwners.SelectedIndex == -1)
            {
                SendMessage("you need to choose one name");
                return;
            }

            MessageBoxResult result = MessageBox.Show("Are you sure to delete?", "CONFIRMATION", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                CarsOwner carsOwnerTobeDeleted = (CarsOwner)LvCarsOwners.SelectedItem;
                Coc.CarsOwnerEntity.Remove(carsOwnerTobeDeleted);
                Coc.SaveChanges();
                ResetValue();
            }
        }
        public static void SendMessage(string msg)
        {
            MessageBox.Show(msg, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public static BitmapImage ByteArrayToBitmapImage(byte[] byteArray)
        {
            BitmapImage bmp = null;
            try
            {
                bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = new MemoryStream(byteArray);
                bmp.EndInit();
            }
            catch
            {
                bmp = null;
            }
            return bmp;
        }

        public static byte[] BitmapImageToByteArray(BitmapImage bmp)
        {
            byte[] byteArray = null;
            try
            {
                Stream sMarket = bmp.StreamSource;
                if (sMarket != null && sMarket.Length > 0)
                {
                    //Very important, because Position is often at the end of the Stream, resulting in a length of 0 read below.
                    sMarket.Position = 0;

                    using (BinaryReader br = new BinaryReader(sMarket))
                    {
                        byteArray = br.ReadBytes((int)sMarket.Length);
                    }
                }
            }
            catch
            {
                //other exception handling 
            }
            return byteArray;
        }

        private void BtnManageCars_Click(object sender, RoutedEventArgs e)
        {
            if (LvCarsOwners.SelectedIndex == -1)
            {
                SendMessage("you need to choose one CarsOwner");
                return;
            }

            CarsOwner selectedCarsOwner = (CarsOwner) LvCarsOwners.SelectedItem;
            Car[] cars = new Car[10];
            string name = selectedCarsOwner.Name;

            CarsOwners windowCars = new CarsOwners(selectedCarsOwner);
            windowCars.RequestResult += (c) =>
            {
                cars = c;
            };

            bool? result = windowCars.ShowDialog();
            if (result == true)
            {
                selectedCarsOwner.Cars = cars;
            }
            Coc.SaveChanges();
            LvCarsOwners.ItemsSource = Coc.CarsOwnerEntity;
            LvCarsOwners.Items.Refresh();
        }

        private void ImgCar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = "c:\\";
            ofd.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == true)
            {
                string selectedFileName = ofd.FileName;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(selectedFileName);
                bitmap.EndInit();
                ImgCar.Source = bitmap;
            }
        }
    }
}
