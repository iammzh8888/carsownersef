﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for CarsOwners.xaml
    /// </summary>
    public partial class CarsOwners : Window
    {
        public event Action<Car[]> RequestResult;
        public CarsOwners(CarsOwner carsOwner)
        {
            InitializeComponent();
        }
    }
}
